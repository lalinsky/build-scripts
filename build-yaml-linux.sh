#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$YAML_VERSION}
DIR=yaml-$VERSION
TARBALL=$DIR.tar.gz
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	curl -LO http://pyyaml.org/download/libyaml/$TARBALL
fi

rm -rf $BUILDDIR/tmp
mkdir -p $BUILDDIR/tmp
cd $BUILDDIR/tmp
tar xf $ROOT/$TARBALL
cd $DIR

ARCH=`uname -m`

rm -rf $BUILDDIR/$DIR-linux-$ARCH-static
./configure --prefix=$BUILDDIR/$DIR-linux-$ARCH-static --enable-static --disable-shared
make
make install

rm -rf $BUILDDIR/tmp

