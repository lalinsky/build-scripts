#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$CHROMAPRINT_VERSION}
DIR=chromaprint-$VERSION
TARBALL=$DIR.tar.gz
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	curl -LO https://bitbucket.org/acoustid/chromaprint/downloads/$TARBALL
fi

for ARCH in i686 x86_64; do

	rm -rf $BUILDDIR/tmp
	mkdir -p $BUILDDIR/tmp
	cd $BUILDDIR/tmp
	tar xf $ROOT/$TARBALL
	cd $DIR

	perl -pe "s!{EXTRA_PATHS}!$BUILDDIR/ffmpeg-$FFMPEG_VERSION-audio-win-$ARCH-static!g" $ROOT/mingw32.cmake.in | perl -pe "s!{ARCH}!$ARCH!g" >mingw32.cmake

	rm -rf $BUILDDIR/$DIR-win-$ARCH-static
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_TOOLCHAIN_FILE=mingw32.cmake \
		-DCMAKE_C_FLAGS='-static -static-libgcc -static-libstdc++' \
		-DCMAKE_CXX_FLAGS='-static -static-libgcc -static-libstdc++' \
		-DCMAKE_INSTALL_PREFIX=$BUILDDIR/$DIR-win-$ARCH-static \
		-DFFMPEG_ROOT=$BUILDDIR/ffmpeg-$FFMPEG_VERSION-audio-win-$ARCH-static \
		-DBUILD_SHARED_LIBS=OFF \
		-DBUILD_EXAMPLES=ON \
		.
	make
	make install
	$ARCH-w64-mingw32-strip $BUILDDIR/$DIR-win-$ARCH-static/bin/fpcalc.exe

	FPCALCDIR=$BUILDDIR/chromaprint-fpcalc-$VERSION-win-$ARCH
	rm -rf $FPCALCDIR
	mkdir $FPCALCDIR
	cp $BUILDDIR/$DIR-win-$ARCH-static/bin/fpcalc.exe $FPCALCDIR

	rm -rf $BUILDDIR/tmp

done

