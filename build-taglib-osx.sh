#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$TAGLIB_VERSION}
DIR=taglib-$VERSION
TARBALL=$DIR.tar.gz
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	curl -LO http://taglib.github.io/releases/$TARBALL
fi

. $ROOT/ffmpeg/common.sh

for ARCH in i386 x86_64; do

	rm -rf $BUILDDIR/tmp
	mkdir -p $BUILDDIR/tmp
	cd $BUILDDIR/tmp
	tar xf $ROOT/$TARBALL
	cd $DIR

	rm -rf $BUILDDIR/$DIR-osx-$ARCH-static
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=$BUILDDIR/$DIR-osx-$ARCH-static \
		-DENABLE_STATIC=ON \
		-DCMAKE_C_COMPILER=$OSX_CC \
		-DCMAKE_CXX_COMPILER=$OSX_CXX \
		-DCMAKE_OSX_SYSROOT=$OSX_SDK \
		-DCMAKE_OSX_DEPLOYMENT_TARGET=$OSX_VERSION \
		-DCMAKE_OSX_ARCHITECTURES=$ARCH \
		.
	make
	make install

	rm -rf $BUILDDIR/tmp

done

