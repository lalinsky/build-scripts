#!/bin/sh

. `dirname $0`/common.sh

./configure \
	$FFMPEG_AUDIO_FLAGS \
	$FFMPEG_OSX_FLAGS \
	--extra-ldflags="-isysroot $OSX_SDK -mmacosx-version-min=$OSX_VERSION -arch $ARCH" \
	--extra-cflags="-isysroot $OSX_SDK -mmacosx-version-min=$OSX_VERSION -arch $ARCH" \
	--enable-shared \
	--disable-static

