#!/bin/sh

. `dirname $0`/common.sh

./configure \
	$FFMPEG_AUDIO_FLAGS \
	$FFMPEG_MINGW32_FLAGS \
	--extra-cflags='-static -static-libgcc -static-libstdc++' \
	--disable-shared \
	--enable-static

