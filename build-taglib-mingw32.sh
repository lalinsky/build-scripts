#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$TAGLIB_VERSION}
DIR=taglib-$VERSION
TARBALL=$DIR.tar.gz
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	curl -LO http://taglib.github.io/releases/$TARBALL
fi

for ARCH in i686 x86_64; do

	rm -rf $BUILDDIR/tmp
	mkdir -p $BUILDDIR/tmp
	cd $BUILDDIR/tmp
	tar xf $ROOT/$TARBALL
	cd $DIR

	perl -pe "s!{EXTRA_PATHS}!!g" $ROOT/mingw32.cmake.in | perl -pe "s!{ARCH}!$ARCH!g" >mingw32.cmake
	perl -pi -e 's{NOT WIN32 AND NOT BUILD_FRAMEWORK}{1}' CMakeLists.txt

	rm -rf $BUILDDIR/$DIR-win-$ARCH-static
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_TOOLCHAIN_FILE=mingw32.cmake \
		-DCMAKE_C_FLAGS='-static-libgcc -static-libstdc++' \
		-DCMAKE_CXX_FLAGS='-static-libgcc -static-libstdc++' \
		-DCMAKE_INSTALL_PREFIX=$BUILDDIR/$DIR-win-$ARCH-static \
		-DENABLE_STATIC=ON \
		.
	make
	make install

	rm -rf $BUILDDIR/tmp

done

