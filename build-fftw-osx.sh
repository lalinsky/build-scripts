#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$FFTW_VERSION}
DIR=fftw-$VERSION
TARBALL=$DIR.tar.gz
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	curl -LO http://www.fftw.org/$TARBALL
fi

. ffmpeg/common.sh

for ARCH in i386 x86_64; do
	export CC=$OSX_CC
	export CFLAGS="-isysroot $OSX_SDK -mmacosx-version-min=$OSX_VERSION -arch $ARCH"
	export LDFLAGS="-isysroot $OSX_SDK -mmacosx-version-min=$OSX_VERSION -arch $ARCH"
	(
		rm -rf $BUILDDIR/tmp &&
		mkdir -p $BUILDDIR/tmp &&
		cd $BUILDDIR/tmp &&
		tar xf $ROOT/$TARBALL &&
		cd $DIR &&
		rm -rf $BUILDDIR/$DIR-osx-$ARCH-static &&
		./configure --host=$ARCH-apple-darwin --prefix=$BUILDDIR/$DIR-osx-$ARCH-static --enable-float --enable-static --disable-shared &&
		make &&
		make install &&
		rm -rf $BUILDDIR/tmp
	)
done

rm -rf $BUILDDIR/tmp
