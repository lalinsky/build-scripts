#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$CHROMAPRINT_VERSION}
DIR=chromaprint-$VERSION
TARBALL=$DIR.tar.gz
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	curl -LO https://bitbucket.org/acoustid/chromaprint/downloads/$TARBALL
fi

rm -rf $BUILDDIR/tmp
mkdir -p $BUILDDIR/tmp
cd $BUILDDIR/tmp
tar xf $ROOT/$TARBALL
cd $DIR

ARCH=`uname -m`

rm -rf $BUILDDIR/$DIR-linux-$ARCH-static
cmake \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_INSTALL_PREFIX=$BUILDDIR/$DIR-linux-$ARCH-static \
	-DFFMPEG_ROOT=$BUILDDIR/ffmpeg-$FFMPEG_VERSION-audio-linux-$ARCH-static \
	-DBUILD_SHARED_LIBS=OFF \
	-DBUILD_EXAMPLES=ON \
	.
make
make install
strip $BUILDDIR/$DIR-linux-$ARCH-static/bin/fpcalc

FPCALCDIR=$BUILDDIR/chromaprint-fpcalc-$VERSION-linux-$ARCH
rm -rf $FPCALCDIR
mkdir $FPCALCDIR
cp $BUILDDIR/$DIR-linux-$ARCH-static/bin/fpcalc $FPCALCDIR

rm -rf $BUILDDIR/tmp

