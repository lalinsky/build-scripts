#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$FFTW_VERSION}
DIR=fftw-$VERSION
TARBALL=$DIR.tar.gz
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	curl -LO http://www.fftw.org/$TARBALL
fi

rm -rf $BUILDDIR/tmp
mkdir -p $BUILDDIR/tmp
cd $BUILDDIR/tmp
tar xf $ROOT/$TARBALL
cd $DIR

ARCH=`uname -m`

rm -rf $BUILDDIR/$DIR-linux-$ARCH-static
./configure --prefix=$BUILDDIR/$DIR-linux-$ARCH-static --enable-float --enable-static --disable-shared
make
make install

rm -rf $BUILDDIR/tmp

