#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$FFMPEG_VERSION}
DIR=ffmpeg-$VERSION
TARBALL=$DIR.tar.bz2
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	wget http://ffmpeg.org/releases/$TARBALL
fi

for BUILD in static shared; do
	(
		rm -rf $BUILDDIR/tmp &&
		mkdir -p $BUILDDIR/tmp &&
		cp $TARBALL $BUILDDIR/tmp &&
		cd $BUILDDIR/tmp &&
		tar xf $TARBALL &&
		cd $DIR &&
		rm -rf $BUILDDIR/$DIR-audio-linux-`uname -m`-$BUILD &&
		PREFIX=$BUILDDIR/$DIR-audio-linux-`uname -m`-$BUILD $ROOT/ffmpeg/ffmpeg-configure-linux-$BUILD.sh &&
		make &&
		make install &&
		rm -rf $BUILDDIR/tmp
	)
done

