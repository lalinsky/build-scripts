#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$TAGLIB_VERSION}
DIR=taglib-$VERSION
TARBALL=$DIR.tar.gz
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	curl -LO http://taglib.github.io/releases/$TARBALL
fi

rm -rf $BUILDDIR/tmp
mkdir -p $BUILDDIR/tmp
cd $BUILDDIR/tmp
tar xf $ROOT/$TARBALL
cd $DIR

ARCH=`uname -m`

rm -rf $BUILDDIR/$DIR-linux-$ARCH-static
cmake \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_INSTALL_PREFIX=$BUILDDIR/$DIR-linux-$ARCH-static \
	-DENABLE_STATIC=ON \
	.
make
make install

rm -rf $BUILDDIR/tmp

