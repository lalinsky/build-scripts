#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$FFMPEG_VERSION}
DIR=ffmpeg-$VERSION
TARBALL=$DIR.tar.bz2
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	curl -O http://ffmpeg.org/releases/$TARBALL
fi

for BUILD in static shared; do
	for ARCH in i386 x86_64; do
	 	(
			rm -rf $BUILDDIR/tmp &&
			mkdir -p $BUILDDIR/tmp &&
			cd $BUILDDIR/tmp &&
			tar xf $ROOT/$TARBALL &&
			cd $DIR &&
			mv configure configure.old &&
			grep -v mach_mach_time_h configure.old >configure &&
			chmod +x configure &&
			rm -rf $BUILDDIR/$DIR-audio-osx-$ARCH-$BUILD &&
			ARCH=$ARCH PREFIX=$BUILDDIR/$DIR-audio-osx-$ARCH-$BUILD $ROOT/ffmpeg/ffmpeg-configure-osx-$BUILD.sh &&
			make &&
			make install &&
			rm -rf $BUILDDIR/tmp
		)
	done
done

