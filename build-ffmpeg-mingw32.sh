#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$FFMPEG_VERSION}
DIR=ffmpeg-$VERSION
TARBALL=$DIR.tar.bz2
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	wget http://ffmpeg.org/releases/$TARBALL
fi

for BUILD in static shared; do
	for ARCH in i686 x86_64; do
		PREFIX=$BUILDDIR/$DIR-audio-win-$ARCH-$BUILD
		(
			rm -rf $BUILDDIR/tmp &&
			mkdir -p $BUILDDIR/tmp &&
			cd $BUILDDIR/tmp &&
			tar xf $ROOT/$TARBALL &&
			cd $DIR &&
			perl -pi -e 's{^(\s*check_cflags -Werror=missing-prototypes\s*)$}{#\1}' configure
			rm -rf $PREFIX &&
			ARCH=$ARCH PREFIX=$PREFIX $ROOT/ffmpeg/ffmpeg-configure-mingw32-$BUILD.sh &&
			make &&
			make install &&
			for lib in `ls $PREFIX/bin/*.lib 2>/dev/null`; do mv $lib $PREFIX/lib/; done &&
			rm -rf $BUILDDIR/tmp
		)
	done
done

