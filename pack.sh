#!/usr/bin/env bash

set -ex

cd `dirname $0`/..

for i in `ls build`; do
	(cd build && tar -zcvf $i.tar.gz $i);
done

