#!/usr/bin/env bash

set -ex

ROOT=$(cd `dirname $0` && pwd)
. $ROOT/versions.sh

VERSION=${VERSION:-$YAML_VERSION}
DIR=yaml-$VERSION
TARBALL=$DIR.tar.gz
BUILDDIR=${BUILDDIR:-$ROOT/../build}

if [ ! -e $TARBALL ]; then
	curl -LO http://pyyaml.org/download/libyaml/$TARBALL
fi

for ARCH in i686 x86_64; do

    rm -rf $BUILDDIR/tmp
    mkdir -p $BUILDDIR/tmp
    cd $BUILDDIR/tmp
    tar xf $ROOT/$TARBALL
    cd $DIR

    export CFLAGS="-DYAML_DECLARE_STATIC"

    rm -rf $BUILDDIR/$DIR-win-$ARCH-static
    ./configure \
        --host=$ARCH-w64-mingw32 \
        --prefix=$BUILDDIR/$DIR-win-$ARCH-static \
        --enable-static \
        --disable-shared
    make
    make install

    rm -rf $BUILDDIR/tmp

done

